#include "portscan.h"

#include <arpa/inet.h>
#include <stdio.h>
#include <sys/socket.h>
#include <unistd.h>
#include <stdint.h>
#include <poll.h>
#include <sys/time.h>

#include <gtk/gtk.h>

#define PROG_NAME "solargal"

#define SOLARMAN_V5_HEADER_LEN      11
#define SOLARMAN_V5_TRAILER_LEN     2

#define BUF_SIZE                    512
#define REGS_TO_READ                206

struct modbus_pkt
{
	uint8_t size;
	uint8_t buf[256];
};

struct modbus_data
{
	uint16_t valid;
	uint16_t val[REGS_TO_READ];
};

struct inverter_data
{
	float    pv1_volts;
	float    pv2_volts;
	float    pv1_amps;
	float    pv2_amps;
	uint16_t pv1_watts;
	uint16_t pv2_watts;
	float    daily_prod;
	uint32_t monthly_prod;
	uint32_t yearly_prod;
	uint32_t total_prod;
	uint32_t alarm_code;
	uint32_t error_code;
	uint16_t usage_now;
	float    usage_today;
	int16_t  batt_watts;
	uint16_t batt_percent;
	uint32_t import_total;
	float    import_today;
	uint32_t export_total;
	float    export_today;
	int16_t  export_now;
	uint16_t temp_inverter;
	uint16_t grid_voltage;
	uint16_t model_num;
};

const char* kstar_alarm_string(unsigned alarm_code)
{
	switch( alarm_code )
	{
		case 0:  return "Grid Volt Low";
		case 1:  return "Grid Volt High";
		case 2:  return "Grid Frequency Low";
		case 3:  return "Grid Frequency High";
		case 4:  return "Solar Loss";
		case 5:  return "Bat Loss";
		case 6:  return "Bat under Voltage";
		case 7:  return "Bat Volt Low";
		case 8:  return "Bat Volt High";
		case 9:  return "Over Load";
		case 10: return "GFCI Over";
		case 11: return "LN Fault";
		case 12: return "Fan Fault";
		case 13: return "Bat CapUnder";
		case 14: return "Bms DisChg Over";
		case 15: return "Bms Chg Over";
		case 16: return "Bms Volt Over";
		case 17: return "Bms Temp Over";
		case 18: return "Bms Dis Temp Low";
		case 19: return "Bms Volt Imbalance";
		case 20: return "Bms Communicate Fault";
		case 21: return "Bms Volt Under";
		case 22: return "Bms Chg Temp Low";
		case 23: return "Bms_VoltHigh";
		case 24: return "Bms_TempHigh";
		case 25: return "Bms_Updating";
		case 26: return "Bms_VersionErr";
		case 27: return "Bms_UpdateFail";
		case 28: return "CT Converse";
		case 29: return "Clock Fail";
		default: return NULL;
	}
}

const char* kstar_error_string(unsigned error_code)
{
	switch( error_code )
	{
		case 0:  return "Soft Time Out";
		case 1:  return "INV Volt Short";
		case 2:  return "GFCI Sensor Fault";
		case 3:  return "ERROR 3 - Undocumented";
		case 4:  return "Bus Low Fault";
		case 5:  return "Bus High Fault";
		case 6:  return "Bus Short Fault";
		case 7:  return "PV ISO Under Fault";
		case 8:  return "PV Input Short";
		case 9:  return "Op Relay Fault";
		case 10: return "INV Curr Over";
		case 11: return "INV DC Over";
		case 12: return "Ambient Over Temp";
		case 13: return "Sink Over Temp";
		case 14: return "Grid Relay Fault";
		case 15: return "DisChg Curr Over";
		case 16: return "Chg Curr Over";
		case 17: return "Current Sensor Fault";
		case 18: return "INV Abnormal";
		case 19: return "EPS Relay Fault";
		case 20: return "Always over load";
		case 31: return "SCI Fault";
		default: return NULL;
	}
}

const char* kstar_model_name(unsigned model_num)
{
	switch( model_num )
	{
		case 0:  return "KSE-2K-048S";
		case 1:  return "KSE-3K-048S";
		case 2:  return "KSE-3.6K-048S";
		case 3:  return "KSE-4.6K-048S";
		case 4:  return "KSE-5K-048S";
		case 5:  return "KSE-3.6K-048";
		case 6:  return "KSE-4.6K-048";
		case 7:  return "KSE-5K-048";
		case 8:  return "KSE-6K-048";
		case 9:  return "BluE-S 3680D";
		case 11: return "BluE-S 5000D";
		case 12: return "BluE-S 6000D";
		case 14: return "KSE-3K-048S M1";
		case 15: return "BluE-S 3680D M1";
		case 17: return "BluE-S 5000D M1";
		case 18: return "BluE-S 6000D M1";
		case 32: return "E10KT";
		case 33: return "E8KT";
		case 34: return "E12KT";
		default: return NULL;
	}
}

typedef struct
{
	struct {                                /* overview tab */
		GtkWidget *label_watts_now;
		GtkWidget *label_usage_now;
		GtkWidget *label_export_now;
		GtkWidget *label_watts_today;
		GtkWidget *label_usage_today;
		GtkWidget *label_export_today;
		GtkWidget *label_import_today;
		GtkWidget *label_batt_watts;
		GtkWidget *label_batt_percent;
	} over;

	struct {                                /* data tab */
		GtkStackPage *stack_page;
		GtkWidget *list;
		GtkWidget *label_ip_addr;
		GtkWidget *label_serial_num;
		GtkWidget *label_model_num;
		GtkWidget *label_grid_voltage;
		GtkWidget *label_watts_total;
		GtkWidget *label_export_total;
	} data;

} gui_widgets_t;


/* global variables */
char     g_ip_addr_inverter[16];
uint32_t g_serial_num = 0;
uint32_t g_event_source_id = 0;
gui_widgets_t g_widgets;


/*
 * Calculate checksum for solarman v5 frame
 */
unsigned char get_checksum(uint8_t* buf, unsigned count)
{
	uint8_t checksum = 0;

	for(unsigned i=0; i<count; i++)
		checksum += buf[i];

	return checksum;
}

void print_solarman_frame(uint8_t *buf)
{
	uint16_t control_code = ( buf[4] << 8 ) | buf[3];

	bool req_pkt  = control_code == 0x4510;
	bool resp_pkt = control_code == 0x1510;

	if ( req_pkt )
		printf("TX ");
	else if ( resp_pkt )
		printf("RX ");
	else
		printf("?? ");

	uint16_t payload_len = ( buf[2] << 8 ) | buf[1];
	uint16_t frame_len = SOLARMAN_V5_HEADER_LEN + payload_len + SOLARMAN_V5_TRAILER_LEN;

	printf("(%d bytes): 0x", frame_len);

	int idx = 0;

	for(int i=0; i<SOLARMAN_V5_HEADER_LEN; i++)
		printf("%02X ", buf[idx++]);

	printf(" ");

	/* print the payload section, wrapping the modbus packet in brackets */
	for(int i=0; i<payload_len; i++)
	{
		if ( req_pkt && (idx == SOLARMAN_V5_HEADER_LEN + 15) )
			printf("(");
		if ( resp_pkt && (idx == SOLARMAN_V5_HEADER_LEN + 14) )
			printf("(");

		printf("%02X", buf[idx++]);

		if ( (req_pkt || resp_pkt) && idx == SOLARMAN_V5_HEADER_LEN + payload_len )
			printf(")");

		printf(" ");
	}

	printf(" ");

	for(int i=0; i<SOLARMAN_V5_TRAILER_LEN; i++)
		printf("%02X ", buf[idx++]);

	printf("\n");
}

/*
 * Calculates the CRC for num_bytes of buf
 */
uint16_t get_crc16(uint8_t* buf, unsigned num_bytes, uint16_t poly, uint16_t init_val)
{
	uint16_t crc = init_val;

	for(unsigned i=0; i<num_bytes; i++)
	{
		crc ^= buf[i];

		for(int j=0; j<8; j++)
		{
			if( crc & 1 )
				crc = (crc >> 1) ^ poly;
			else
				crc = (crc >> 1);
		}
	}

	return crc;
}

/*
 * Calculates the Modbus CRC for num_bytes of buf
 */
uint16_t get_modbus_crc(uint8_t* buf, unsigned num_bytes)
{
	return get_crc16(buf, num_bytes, 0xA001, 0xFFFF);
}

uint64_t millis()
{
	struct timeval t;
	gettimeofday(&t, NULL);
	return t.tv_sec * 1000LL + t.tv_usec / 1000;
}

void sleep_ms(unsigned count)
{
	struct timespec rem = {0};
	struct timespec req = { count / 1000, (count % 1000) * 1000 * 1000 };
	nanosleep(&req , &rem);
}

/*
 * Block until fd becomes writable or timeout expires
 *
 * Returns:
 *      0 on timeout
 *     >0 if data is available
 *     -1 on error
 */
int wait_for_writable(int fd, int timeout_ms)
{
	struct pollfd fds[2];

	fds[0].fd = fd;
	fds[0].events = POLLOUT;

	return poll(fds, 1, timeout_ms);
}

/*
 * Try to write count bytes to the socket
 *
 * Returns the number of bytes written or -1 on error
 */
int socket_write(int fd, uint8_t *buf, unsigned count, unsigned timeout_ms)
{
	uint64_t start = millis();

	unsigned bytes_written = 0;

	while ( bytes_written < count )
	{
		unsigned bytes_to_write = count - bytes_written;

		int status = write(fd, buf, bytes_to_write);

		if ( status < 0 )
		{
			if ( errno != EAGAIN && errno != EWOULDBLOCK )
			{
				fprintf(stderr, "write() to socket gave error:%d errno:%d\n", status, errno);
				return -1;
			}
		}
		else
		{
			bytes_written += status;
		}

		if ( millis() - start > timeout_ms )
			break;
	}

	return bytes_written;
}

/*
 * Block until data arrives or timeout expires
 *
 * Returns:
 *      0 on timeout
 *     >0 if data is available
 *     -1 on error
 */
int wait_for_data(int fd, int timeout_ms)
{
	struct pollfd fds[2];

	fds[0].fd = fd;
	fds[0].events = POLLIN;

	return poll(fds, 1, timeout_ms);
}

/*
 * Reads the modbus input or holding registers
 *
 * Returns
 *       0 on success
 *      -1 on error
 *       detected serial number if passed serial num is 0
 */
int read_registers(
				int sock_fd,
				uint32_t serial_num,
				uint8_t func,
				uint16_t reg,
				uint16_t count,
				uint16_t *p_out)
{
	if ( count < 1 )
	{
		printf("ERROR: count cannot be zero\n");
		return -1;
	}

	struct modbus_pkt pkt = {0};
	pkt.buf[0] = 0x1;
	pkt.buf[1] = func;
	pkt.buf[2] = 0xFF & (reg >> 8);
	pkt.buf[3] = 0xFF & (reg >> 0);
	pkt.buf[4] = 0xFF & (count >> 8);
	pkt.buf[5] = 0xFF & (count >> 0);

	uint16_t crc = get_modbus_crc(pkt.buf, 6);

	pkt.buf[6] = 0xFF & (crc >> 0);
	pkt.buf[7] = 0xFF & (crc >> 8);

	pkt.size   = 8;

	uint16_t payload_len = 15 + pkt.size;

	uint16_t control_code = 0x4510;

	uint8_t buf[BUF_SIZE];
	int idx = 0;

	/* build header */
	buf[idx++] = 0xA5;
	buf[idx++] = 0xFF & (payload_len  >> 0);
	buf[idx++] = 0xFF & (payload_len  >> 8);
	buf[idx++] = 0xFF & (control_code >> 0);
	buf[idx++] = 0xFF & (control_code >> 8);
	buf[idx++] = 0;
	buf[idx++] = 0;
	buf[idx++] = 0xFF & (serial_num >>  0);
	buf[idx++] = 0xFF & (serial_num >>  8);
	buf[idx++] = 0xFF & (serial_num >> 16);
	buf[idx++] = 0xFF & (serial_num >> 24);

	/* build payload */
	buf[idx++] = 0x2;  /* solar inverter (frame-type 2) */
	buf[idx++] = 0;    /* sensor type */
	buf[idx++] = 0;
	buf[idx++] = 0;    /* delivery time */
	buf[idx++] = 0;
	buf[idx++] = 0;
	buf[idx++] = 0;
	buf[idx++] = 0;    /* power on time */
	buf[idx++] = 0;
	buf[idx++] = 0;
	buf[idx++] = 0;
	buf[idx++] = 0;    /* offset time */
	buf[idx++] = 0;
	buf[idx++] = 0;
	buf[idx++] = 0;

	for(int i=0; i<pkt.size; i++)
		buf[idx++] = pkt.buf[i];

	uint8_t checksum = get_checksum(buf+1, idx-1);
	buf[idx++] = checksum;
	buf[idx++] = 0x15;

	//print_solarman_frame(buf);

	int status = socket_write(sock_fd, buf, idx, 200);

	if ( status != idx )
		return -1;

	/* wait for a response */
	status = wait_for_data(sock_fd, 5000);

	if ( status == 0 )
	{
		fprintf(stderr, "ERROR: wait_for_data(1) timed out\n");
		return -1;
	}

	if ( status < 0 )
	{
		fprintf(stderr, "ERROR: wait_for_data(1) returned error %d\n", status);
		return -1;
	}

	/* read the response */
	idx = 0;
	while( true )
	{
		unsigned max_bytes = BUF_SIZE-idx;

		if( max_bytes == 0 )
		{
			fprintf(stderr, "ERROR: read max bytes\n");
			break;
		}

		int status = read(sock_fd, buf+idx, max_bytes);

		if ( status < 1 )
		{
			if ( errno != EAGAIN )
			{
				fprintf(stderr, "ERROR: reading from socket gave error:%d errno:%d\n", status, errno);
				break;
			}
		}
		else
		{
			idx += status;
		}

		/* wait a little for more data */
		status = wait_for_data(sock_fd, 100);
		if ( status == 0 )
			break;

		if ( status < 0 )
		{
			fprintf(stderr, "ERROR: wait_for_data(2) returned error %d\n", status);
			return -1;
		}
	}

	//print_solarman_frame(buf);

	if ( idx == 29 && serial_num == 0 )
	{
		uint32_t *p_val = (uint32_t *) (buf+7);
		fprintf(stderr, "Detected solarman serial# 0x%08X\n", *p_val);
		return *p_val;
	}

	if ( idx < 32 )
	{
		fprintf(stderr, "ERROR: bad response data, num:bytes:%d\n", idx);
		return -1;
	}

	if ( buf[12] != 1 )
	{
		fprintf(stderr, "ERROR: bad response status:0x%X\n", buf[12]);
		return -1;
	}

	payload_len = ( buf[2] << 8 ) | buf[1];

	checksum = get_checksum(buf+1, SOLARMAN_V5_HEADER_LEN + payload_len - 1);
	uint8_t pkt_checksum = buf[idx-2];
	if ( pkt_checksum != checksum )
	{
		fprintf(stderr, "ERROR: bad checksum. PKT:0x%02x COMPUTED:0x%02X\n", pkt_checksum, checksum);
		return -1;
	}

	uint8_t modbus_func = buf[ SOLARMAN_V5_HEADER_LEN+14+1 ];
	if ( modbus_func & 0x80 )
	{
		fprintf(stderr, "ERROR: modbus exception:0x%X\n", modbus_func);
		return -1;
	}

	/* modbus uses 1 byte for data size, which limits the number of regs, so get from payload_len */
	uint16_t data_bytes = payload_len - 14 -5;
	if ( data_bytes / 2 != count )
	{
		fprintf(stderr, "ERROR: incorrect number of data bytes:%d count:%d\n", data_bytes, count);
		return -1;
	}

	uint16_t *p_val = (uint16_t*) (buf+28);

	for(int i=0; i<count; i++)
	{
		if ( p_out )
			p_out[i] = ntohs( p_val[i] );
	}

	return 0;
}

int setup_socket(const char* addr, unsigned port)
{
	/* create socket */
	int sock_fd = socket(AF_INET, SOCK_STREAM | SOCK_NONBLOCK, 0);

	if (sock_fd == -1)
	{
		fprintf(stderr, "socket creation failed, errno:%d\n", errno);
		return -1;
	}

	/* setup server address */
	struct sockaddr_in serv_addr = {0};
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = inet_addr(addr);
	serv_addr.sin_port = htons(port);

	/* connect to server */
	int status = connect(sock_fd, (struct sockaddr*)&serv_addr, sizeof(serv_addr));

	if ( status != -1 || errno != EINPROGRESS )
	{
		fprintf(stderr, "connection with the server failed with status:%d errno:%d\n", status, errno);
		return -1;
	}

	/* non-blocking socket so wait until writable before checking if connect has succeeded */
	status = wait_for_writable(sock_fd, 1500);

	if ( status <= 0 )
	{
		fprintf(stderr, "socket connect never completed (not-writable), status:%d\n", status);
		return -1;
	}

	int error = 0;
	socklen_t len = sizeof(int);
	status = getsockopt(sock_fd, SOL_SOCKET, SO_ERROR, &error, &len);

	if ( status != 0 || error != 0 )
	{
		fprintf(stderr, "socket connect never completed (so-error), status:%d error:%d\n", status, error);
		return -1;
	}

	return sock_fd;
}

/*
 * Send a request to the inverter and extract the serial number from the response
 */
uint32_t detect_serial_num(int sock_fd)
{
	uint16_t val;

	int serial_num = read_registers(sock_fd, 0, 4, 0, 1, &val);

	if ( serial_num == -1 )
	{
		fprintf(stderr, "ERROR: failed to detect serial number\n");
		return 0;
	}

	return serial_num;
}

/*
 * Try to detect inverter ip address and serial number
 *
 * Returns a valid socket descriptor on success
 */
int find_inverter(char *p_addr_inverter, uint32_t *p_serial_num)
{
	struct portscan_data port_data = {0};

	/* find all local ip addresses with an open port */
	int status = portscan(&port_data, 8899);
	if ( status )
	{
		return -1;
	}

	/* return the first socket where we detect a serial number and close the others */
	int ret_fd = -1;
	int inverter_detected = 0;
	for(int i=0; i<256; i++)
	{
		if ( port_data.sock_fds[i] >= 0 )
		{
			if ( ! inverter_detected )
			{
				uint32_t serial_num = detect_serial_num( port_data.sock_fds[i] );
				if ( serial_num )
				{
					*p_serial_num = serial_num;
					sprintf(p_addr_inverter, "%s%d", port_data.ip_range, i);
					inverter_detected = 1;
					ret_fd = port_data.sock_fds[i];
					continue;
				}
			}

			close( port_data.sock_fds[i] );
		}
	}

	return ret_fd;
}

void reg_scan(int sock_fd)
{
	uint16_t val[2];

	for(int i=3000; i<3600; i++)
	{
		int reg = 0 + i;

		int success = 0;

		for(int j=0; j<3; j++)
		{
			int status = read_registers(sock_fd, g_serial_num, 4, reg, 2, val);

			if ( status != 0 )
			{
				fprintf(stderr, "ERROR: read_registers() returned %d for reg %d\n", status, reg);
			}
			else
			{
				success = 1;
				break;
			}

			sleep_ms(5);
		}

		if ( ! success )
			break;

		printf("r%05d\t0x%04X\t%05d\n",
			reg, val[0], val[0]);
	}

	fprintf(stderr, "DONE\n");
}

void print_inverter_data(struct inverter_data *iv_data)
{
	printf("\nCurrent Output: %6uW\n", iv_data->pv1_watts + iv_data->pv2_watts);

	printf("\nDaily Production: %6.1fkWh\n", iv_data->daily_prod);

	printf("\nMonthly Production: %6ukWh\n", iv_data->monthly_prod);

	printf("\nYearly Production: %6ukWh\n", iv_data->yearly_prod);

	printf("\nCumulative Production: %6ukWh\n", iv_data->total_prod);

	printf("\nCumulative Energy Purchased: %6ukWh\n", iv_data->import_total);

	printf("\nCumulative Grid feed-in: %ukWh\n", iv_data->export_total);

	printf("\nTotal consumption power: %uW\n", iv_data->usage_now);

	printf("\nChassis Internal Temp: %u C\n", iv_data->temp_inverter);

	printf("\nAlarm Code: 0x%08X\n", iv_data->alarm_code);
	for(int i=0; i<32; i++)
	{
		if ( iv_data->alarm_code & (1 << i) )
		{
			const char *s = kstar_alarm_string(i);
			if ( s )
				printf("\tW%02d %s\n", i, s);
		}
	}

	printf("\nError Code: 0x%08X\n", iv_data->error_code);
	for(int i=0; i<32; i++)
	{
		if ( iv_data->error_code & (1 << i) )
		{
			const char *s = kstar_error_string(i);
			if ( s )
				printf("\tF%02d %s\n", i, s);
		}
	}
}

/*
 * Populate the inverter data struct using the modbus data
 */
int parse_inverter_data(struct modbus_data *mbdata, struct inverter_data *inverter_data)
{
	if ( ! mbdata->valid )
		return -1;

	inverter_data->pv1_volts     = mbdata->val[0] / 10.0;
	inverter_data->pv2_volts     = mbdata->val[1] / 10.0;

	inverter_data->pv1_amps      = mbdata->val[12] / 100.0;
	inverter_data->pv2_amps      = mbdata->val[13] / 100.0;

	inverter_data->pv1_watts     = mbdata->val[24];
	inverter_data->pv2_watts     = mbdata->val[25];

	inverter_data->daily_prod    = ( mbdata->val[35] << 16 ) | mbdata->val[36];
	inverter_data->monthly_prod  = ( mbdata->val[37] << 16 ) | mbdata->val[38];
	inverter_data->yearly_prod   = ( mbdata->val[39] << 16 ) | mbdata->val[40];
	inverter_data->total_prod    = ( mbdata->val[41] << 16 ) | mbdata->val[42];
	inverter_data->daily_prod   /= 10.0;
	inverter_data->total_prod   /= 10;

	inverter_data->model_num     = mbdata->val[45];

	inverter_data->alarm_code    = ( mbdata->val[49] << 16 ) | mbdata->val[50];
	inverter_data->error_code    = ( mbdata->val[51] << 16 ) | mbdata->val[52];

	inverter_data->grid_voltage  = mbdata->val[97];
	inverter_data->grid_voltage /= 10;

	inverter_data->export_today  = mbdata->val[116];
	inverter_data->export_today /= 10.0;

	inverter_data->import_today  = mbdata->val[109];
	inverter_data->import_today /= 10.0;

	inverter_data->export_total  = ( mbdata->val[121] << 16 ) | mbdata->val[122];
	inverter_data->export_total /= 10;

	inverter_data->import_total  = ( mbdata->val[114] << 16 ) | mbdata->val[115];
	inverter_data->import_total /= 10;

	inverter_data->usage_now     = mbdata->val[144];
	inverter_data->usage_today   = mbdata->val[147];
	inverter_data->usage_today  /= 10.0;

	inverter_data->batt_watts    = mbdata->val[65];
	inverter_data->batt_percent  = mbdata->val[66] / 10;

	inverter_data->export_now =
				(inverter_data->pv1_watts + inverter_data->pv2_watts ) +
				inverter_data->batt_watts -
				inverter_data->usage_now;

	inverter_data->temp_inverter = mbdata->val[57] / 10;

	return 0;
}

void get_app_data(int sock_fd, uint32_t serial_num, struct modbus_data *data)
{
	int reg = 3000;

	/* try the read up to 3 times if needed */
	for(int i=0; i<3; i++)
	{
		int status = read_registers(sock_fd, serial_num, 4, reg, REGS_TO_READ, data->val);

		if ( status != 0 )
		{
			fprintf(stderr, "ERROR: read_registers() returned %d for reg %d count:%u\n",
						status, reg, REGS_TO_READ);
		}
		else
		{
			data->valid = 1;
			break;
		}

		sleep_ms(5);
	}
}

/*
 * Hide errors and warnings in the events list if not currently active
 */
void filter_event_list(uint32_t error_bits, uint32_t warn_bits, GtkWidget *list)
{
	GtkListBoxRow *row;
	unsigned list_idx = 0;
	unsigned count = 0;

	for(int i=0; i<32; i++)
	{
		int active = error_bits & (1 << i);
		row = gtk_list_box_get_row_at_index (GTK_LIST_BOX(list), list_idx);
		gtk_widget_set_visible( GTK_WIDGET(row), active );
		list_idx++;
		if ( active )
			count++;
	}

	for(int i=0; i<32; i++)
	{
		int active = warn_bits & (1 << i);
		row = gtk_list_box_get_row_at_index (GTK_LIST_BOX(list), list_idx);
		gtk_widget_set_visible( GTK_WIDGET(row), active );
		list_idx++;
		if ( active )
			count++;
	}

	row = gtk_list_box_get_row_at_index (GTK_LIST_BOX(list), list_idx);
	gtk_widget_set_visible( GTK_WIDGET(row), count == 0 );
}

/*
 * Returns true if an error or a non-normal warning is logged
 */
gboolean is_notable_event(struct inverter_data *iv_data)
{
	if ( iv_data->error_code )
		return true;

	for(int i=0; i<32; i++)
	{
		switch(i)
		{
			case 7:   //normal events
			case 13:
			case 16:
				break;
			default:
				if ( iv_data->alarm_code & (1 << i) )
					return true;
		}
	}

	return false;
}

/*
 * Update the gui widgets using the passed inverter data
 */
void update_gui(struct inverter_data *iv_data, gui_widgets_t *p_widgets)
{
	char str[50];
	GtkWidget *label;

	sprintf(str, "Generating %u Watts", iv_data->pv1_watts + iv_data->pv2_watts);
	label = p_widgets->over.label_watts_now;
	gtk_label_set_text( GTK_LABEL(label), str);

	sprintf(str, "Using %u Watts", iv_data->usage_now);
	label = p_widgets->over.label_usage_now;
	gtk_label_set_text( GTK_LABEL(label), str);

	if ( iv_data->batt_watts < 0 )
		sprintf(str, "Charging (%d Watts)",   -iv_data->batt_watts);
	else if ( iv_data->batt_watts > 0 )
		sprintf(str, "Discharging (%d Watts)", iv_data->batt_watts);
	else
		sprintf(str, "Idle");
	label = p_widgets->over.label_batt_watts;
	gtk_label_set_text( GTK_LABEL(label), str);

	sprintf(str, "%u%%", iv_data->batt_percent);
	label = p_widgets->over.label_batt_percent;
	gtk_label_set_text( GTK_LABEL(label), str);

	sprintf(str, "%.1f kWh today", iv_data->usage_today);
	label = p_widgets->over.label_usage_today;
	gtk_label_set_text( GTK_LABEL(label), str);

	sprintf(str, "%.1f kWh today", iv_data->daily_prod);
	label = p_widgets->over.label_watts_today;
	gtk_label_set_text( GTK_LABEL(label), str);

	if ( iv_data->export_now > 0 )
		sprintf(str, "Exporting %d Watts", iv_data->export_now);
	else if ( iv_data->export_now < 0 )
		sprintf(str, "Importing %d Watts", -iv_data->export_now);
	else
		sprintf(str, "Idle");
	label = p_widgets->over.label_export_now;
	gtk_label_set_text( GTK_LABEL(label), str);

	sprintf(str, "%.1f kWh exported today", iv_data->export_today);
	label = p_widgets->over.label_export_today;
	gtk_label_set_text( GTK_LABEL(label), str);

	filter_event_list(iv_data->error_code, iv_data->alarm_code, p_widgets->data.list);
	gtk_stack_page_set_needs_attention(
			p_widgets->data.stack_page, is_notable_event(iv_data));

	sprintf(str, "Inverter IP: %s", g_ip_addr_inverter);
	label = p_widgets->data.label_ip_addr;
	gtk_label_set_text( GTK_LABEL(label), str);

	sprintf(str, "Inverter Serial: 0x%X", g_serial_num);
	label = p_widgets->data.label_serial_num;
	gtk_label_set_text( GTK_LABEL(label), str);

	sprintf(str, "Grid Voltage: %uV", iv_data->grid_voltage);
	label = p_widgets->data.label_grid_voltage;
	gtk_label_set_text( GTK_LABEL(label), str);

	sprintf(str, "Inverter Model: %s", kstar_model_name(iv_data->model_num));
	label = p_widgets->data.label_model_num;
	gtk_label_set_text( GTK_LABEL(label), str);

	sprintf(str, "Total generation: %u kWh", iv_data->total_prod);
	label = p_widgets->data.label_watts_total;
	gtk_label_set_text( GTK_LABEL(label), str);

	sprintf(str, "Total export: %u kWh", iv_data->export_total);
	label = p_widgets->data.label_export_total;
	gtk_label_set_text( GTK_LABEL(label), str);
}

/*
 * Fetch data from inverter and update the ui
 * Returns TRUE so that g_timeout_add_seconds() will continue to call it periodically
 */
static gboolean update_values(gpointer user_data)
{
	int sock_fd;

	if ( g_serial_num == 0 )
		sock_fd = find_inverter(g_ip_addr_inverter, &g_serial_num);
	else
		sock_fd = setup_socket(g_ip_addr_inverter, 8899);

	if ( sock_fd < 0 )
	{
		fprintf(stderr, "Failed to setup socket, serial:0x%X\n", g_serial_num);
		return TRUE;
	}

	if ( ! g_serial_num )
	{
		g_serial_num = detect_serial_num(sock_fd);

		if ( ! g_serial_num )
		{
			fprintf(stderr, "ERROR: failed to detect serial number\n");
			close(sock_fd);
			return TRUE;
		}
	}

	struct modbus_data mb_data = {0};
	get_app_data(sock_fd, g_serial_num, &mb_data);
	close(sock_fd);

	if ( mb_data.valid )
	{
		struct inverter_data iv_data = {0};
		parse_inverter_data(&mb_data, &iv_data);
		print_inverter_data(&iv_data);

		gui_widgets_t *p_widgets = (gui_widgets_t*) user_data;
		update_gui(&iv_data, p_widgets);
	}
	else
	{
		fprintf(stderr, "ERROR: failed to get valid modbus data\n");
	}

	return TRUE;
}

/*
 * React to window getting/losing focus
 */
static void on_focus_change(gpointer data)
{
	gui_widgets_t *p_widgets = (gui_widgets_t*) data;
	GtkWidget *label = p_widgets->over.label_watts_now;

	GtkRoot *root = gtk_widget_get_root (GTK_WIDGET (label));
	GtkWindow *window = GTK_WINDOW (root);

	/* kill any existing timeout */
	if ( g_event_source_id > 0 )
		g_source_remove(g_event_source_id);

	if ( gtk_window_is_active(window) )
		g_event_source_id = g_timeout_add_seconds(5, update_values, p_widgets);
	else
		g_event_source_id = g_timeout_add_seconds(300, update_values, p_widgets);
}

GtkWidget *try_load_image(gchar *app_name, gchar* file_name)
{
	gchar buf[100];

	sprintf(buf, "../data/%s", file_name);
	if ( g_file_test (buf, G_FILE_TEST_EXISTS) )
		return gtk_image_new_from_file (buf);

	sprintf(buf, "/usr/local/share/%s/%s", app_name, file_name);
	if ( g_file_test (buf, G_FILE_TEST_EXISTS) )
		return gtk_image_new_from_file (buf);

	sprintf(buf, "/usr/share/%s/%s", app_name, file_name);
	if ( g_file_test (buf, G_FILE_TEST_EXISTS) )
		return gtk_image_new_from_file (buf);

	return gtk_image_new_from_file("not found");
}

void expand_all_children(GtkWidget *widget)
{
	GtkWidget *child = gtk_widget_get_first_child(widget);
	while(child)
	{
		gtk_widget_set_vexpand (child, true);
		gtk_widget_set_hexpand (child, true);
		child = gtk_widget_get_next_sibling(child);
	}
}

GtkWidget *make_grid_box(GtkWidget *top, GtkWidget *bottom)
{
	GtkWidget *box = gtk_box_new (GTK_ORIENTATION_VERTICAL, 20);

	gtk_box_append ( (GtkBox*)box, top );
	gtk_box_append ( (GtkBox*)box, bottom );

	expand_all_children(box);

	gtk_widget_set_valign(top, GTK_ALIGN_END);
	gtk_widget_set_valign(bottom, GTK_ALIGN_START);

	return box;
}

void create_window(GtkApplication *app)
{
	char str[30];

	GtkWidget *window = gtk_application_window_new (app);
	gtk_window_set_title (GTK_WINDOW (window), "SolarGal");
	gtk_window_set_default_size (GTK_WINDOW (window), 400, 800);

	gui_widgets_t *p_widgets = &g_widgets;

	/* setup overview page */
	p_widgets->over.label_watts_now    = gtk_label_new (NULL);
	p_widgets->over.label_usage_now    = gtk_label_new (NULL);
	p_widgets->over.label_export_now   = gtk_label_new (NULL);
	p_widgets->over.label_watts_today  = gtk_label_new (NULL);
	p_widgets->over.label_usage_today  = gtk_label_new (NULL);
	p_widgets->over.label_export_today = gtk_label_new (NULL);
	p_widgets->over.label_batt_watts   = gtk_label_new (NULL);
	p_widgets->over.label_batt_percent = gtk_label_new (NULL);

	GtkWidget *prod_box = make_grid_box(
									p_widgets->over.label_watts_now,
									p_widgets->over.label_watts_today);

	GtkWidget *batt_box = make_grid_box(
									p_widgets->over.label_batt_watts,
									p_widgets->over.label_batt_percent);

	GtkWidget *usage_box = make_grid_box(
									p_widgets->over.label_usage_now,
									p_widgets->over.label_usage_today);

	GtkWidget *export_box = make_grid_box(
									p_widgets->over.label_export_now,
									p_widgets->over.label_export_today);

	GtkWidget *grid = gtk_grid_new();
	gtk_grid_attach ( (GtkGrid*)grid, prod_box,   1, 0, 1, 1 );
	gtk_grid_attach ( (GtkGrid*)grid, batt_box,   1, 1, 1, 1 );
	gtk_grid_attach ( (GtkGrid*)grid, usage_box,  1, 2, 1, 1 );
	gtk_grid_attach ( (GtkGrid*)grid, export_box, 1, 3, 1, 1 );

	GtkWidget *image = try_load_image(PROG_NAME, "solar.svg");
	gtk_widget_set_margin_start(image, 20);
	gtk_grid_attach ( (GtkGrid*)grid, image,    0, 0, 1, 1 );
	image = try_load_image(PROG_NAME, "battery.svg");
	gtk_widget_set_margin_start(image, 20);
	gtk_grid_attach ( (GtkGrid*)grid, image,    0, 1, 1, 1 );
	image = try_load_image(PROG_NAME, "house.svg");
	gtk_widget_set_margin_start(image, 20);
	gtk_grid_attach ( (GtkGrid*)grid, image,    0, 2, 1, 1 );
	image = try_load_image(PROG_NAME, "grid.svg");
	gtk_widget_set_margin_start(image, 20);
	gtk_grid_attach ( (GtkGrid*)grid, image,    0, 3, 1, 1 );

	expand_all_children(grid);

	/* setup data page */
	p_widgets->data.list = gtk_list_box_new ();
	for(int i=0; i<32; i++)
	{
		const char *s = kstar_error_string(i);
		sprintf(str, "F%02d %s\n", i, s);
		GtkWidget *label = gtk_label_new (str);
		gtk_list_box_append ( GTK_LIST_BOX(p_widgets->data.list), label );
	}

	for(int i=0; i<32; i++)
	{
		const char *s = kstar_alarm_string(i);
		sprintf(str, "W%02d %s\n", i, s);
		GtkWidget *label = gtk_label_new (str);
		gtk_list_box_append ( GTK_LIST_BOX(p_widgets->data.list), label );
	}

	GtkWidget *label = gtk_label_new ("No active events");
	gtk_list_box_append ( GTK_LIST_BOX(p_widgets->data.list), label );

	p_widgets->data.label_ip_addr      = gtk_label_new (NULL);
	p_widgets->data.label_serial_num   = gtk_label_new (NULL);
	p_widgets->data.label_grid_voltage = gtk_label_new (NULL);
	p_widgets->data.label_model_num    = gtk_label_new (NULL);
	p_widgets->data.label_watts_total  = gtk_label_new (NULL);
	p_widgets->data.label_export_total = gtk_label_new (NULL);

	GtkWidget *scroll_box = gtk_box_new (GTK_ORIENTATION_VERTICAL, 20);
	gtk_box_append ( (GtkBox*)scroll_box, p_widgets->data.list );
	gtk_box_append ( (GtkBox*)scroll_box, p_widgets->data.label_ip_addr );
	gtk_box_append ( (GtkBox*)scroll_box, p_widgets->data.label_serial_num );
	gtk_box_append ( (GtkBox*)scroll_box, p_widgets->data.label_model_num );
	gtk_box_append ( (GtkBox*)scroll_box, p_widgets->data.label_grid_voltage );
	gtk_box_append ( (GtkBox*)scroll_box, p_widgets->data.label_watts_total );
	gtk_box_append ( (GtkBox*)scroll_box, p_widgets->data.label_export_total );

	GtkWidget *scrollview = gtk_scrolled_window_new();
	gtk_scrolled_window_set_child( GTK_SCROLLED_WINDOW(scrollview), scroll_box );
	gtk_widget_set_vexpand (scrollview, true);

	GtkWidget *data_box = gtk_box_new (GTK_ORIENTATION_VERTICAL, 20);
	gtk_box_append ( (GtkBox*)data_box, scrollview );

	/* setup main stack */
	GtkStack *stack = GTK_STACK(gtk_stack_new ());
	gtk_stack_add_titled(stack, grid,       NULL, "Overview");
	gtk_stack_add_titled(stack, data_box, NULL, "Data");

	p_widgets->data.stack_page = gtk_stack_get_page(stack, GTK_WIDGET(data_box));

	GtkStackSwitcher *switcher = GTK_STACK_SWITCHER(gtk_stack_switcher_new ());
	gtk_widget_set_halign (GTK_WIDGET(switcher), GTK_ALIGN_CENTER);
	gtk_stack_switcher_set_stack (switcher, stack);

	GtkWidget *stack_box = gtk_box_new (GTK_ORIENTATION_VERTICAL, 20);
	gtk_box_append ( (GtkBox*)stack_box, GTK_WIDGET(switcher) );
	gtk_box_append ( (GtkBox*)stack_box, GTK_WIDGET(stack) );

	gtk_window_set_child (GTK_WINDOW (window), stack_box);

	update_values(p_widgets);

	/* this creates a timeout which calls update_values() periodically */
	on_focus_change(p_widgets);

	g_signal_connect_swapped (
		window, "notify::is-active", G_CALLBACK (on_focus_change), (gpointer)p_widgets);

	gtk_widget_set_visible (window, true);
}

static void activate (GtkApplication *app, gpointer user_data)
{
	(void) user_data; /* unused */

	GList *list = gtk_application_get_windows (app);

	if (list)
		gtk_window_present (GTK_WINDOW (list->data));
	else
		create_window(app);
}

int main(int argc, char *argv[])
{
	if ( argc > 1 )
	{
		int sock_fd = find_inverter(g_ip_addr_inverter, &g_serial_num);
		if ( sock_fd < 0 )
			return -1;
		reg_scan(sock_fd);
		close(sock_fd);
		return 0;
	}

	GtkApplication* app = gtk_application_new ("org.shiftee.solargal", G_APPLICATION_DEFAULT_FLAGS);
	g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);
	int status = g_application_run (G_APPLICATION (app), argc, argv);
	g_object_unref (app);

	return status;
}

