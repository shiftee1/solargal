# Description
A simple GTK app to retrieve and display information from a KSTAR Solar Inverter.
It also supports rebranded KSTAR inverters such as Ergocell.
It fetches the info using Modbus over the Solarman V5 protocol.


# Dependencies
sudo apt install meson libgtk-4-dev


# Build instructions

```bash
meson setup build
cd build
meson compile
meson install
```

# Screenshot
![ALT](https://gitlab.com/shiftee1/solargal/uploads/4738006f078880406f8c4d00515ee1f5/Screenshot_from_2023-09-03_12-39-21.png)
